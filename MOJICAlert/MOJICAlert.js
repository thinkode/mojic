/*Cette classe permet l'utilisation des UIAlerts
V.BOULANGER - © 2021
Version 1.0
*/

const   MOJICALERT_DANGER      = { pattern: "danger.png", color: "#FF0000" };
const   MOJICALERT_WARNING     = { pattern: "warning.png", color: "#FBB03B" };
const   MOJICALERT_SUCCESS     = { pattern: "success.png", color: "#00FF00" };

//Inscription de la police Space Age
var fontStyle = document.createElement('style');
fontStyle.appendChild(document.createTextNode("\
@font-face {\
    font-family: Space_Age;\
    src: url('./MOJICAlert/space_age.ttf');\
}\
"));
document.head.appendChild(fontStyle);

function MOJICAlert(title, subtitle, alertType){
    var that = this;
    let chrono = false;
    this.title = title;
    this.subtitle = subtitle;
    this.pattern = alertType;
    this.show = function(){
        let notificationTemplate = createNotificationBody(this.pattern.pattern);
        let titleDesc = document.createElement("p");
        titleDesc.setAttribute("style", "color: " + this.pattern.color + "; font-family: 'Space_Age'; text-align:center; line-height:20px; font-size:45px;");
        titleDesc.innerHTML = this.title;
        notificationTemplate.appendChild(titleDesc);
        let subtitleDesc = document.createElement("h1");
        subtitleDesc.setAttribute("style", "color: " + this.pattern.color + "; font-family: 'Space_Age'; text-align:center;");
        subtitleDesc.innerHTML = this.subtitle;
        notificationTemplate.appendChild(subtitleDesc);
        this.hide();
        document.body.appendChild(notificationTemplate);
        console.log("affiché");
    };
    this.setBlinkerOn = function(duration = 500){
        chrono = true;
        this.launchBlinker(duration);
    };
    this.setBlinkerOff = function(duration = 500){
        chrono = false;
    };
    this.hide = function(){
        let uiAlert = document.getElementById("uiAlert");
        if(uiAlert) uiAlert.remove();
    };
    this.launchBlinker = function(duration){
        if(chrono){
            that.show();
            setTimeout(function(){
                that.hide();
                setTimeout(function(){
                    that.launchBlinker(duration);
                }, duration);
            }, duration);
        }
    }
}

//Crée le corps de la notification
function createNotificationBody(patternType){
    let template = document.createElement("div");
    template.setAttribute("id", "uiAlert");
    template.setAttribute("style", "z-index: 9999;background-image: url('MOJICAlert/256w/" + patternType + "'); position:absolute; width:100%; height: 172px; padding:0; background-color: black; opacity:75%; margin: auto;background-repeat: repeat-x;top: 0;right: 0;bottom: 0;left: 0;");
    return template;
}

console.log("[MOJIC] Le composant MOJICAlert est prêt à être utilisé");